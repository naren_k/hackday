/**
 * 
 */
package com.hackday.timexchange.dryrun;

/**
 * @author kumblen
 * 
 */
public class DryRun {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		String periodName = args[0];
		String userId = args[1];

		System.out.println("Starting DryRun for period" + periodName + " with userId " + userId);

		DryRunSteps steps = new DryRunSteps();

		// System Admin Black Out
		steps.invokeSystemAdminBlackout();
		
		steps.runFinanacialProcess(periodName);
		
		steps.fetchNonTMEstimatedAccruals();
		
		steps.fetchPhyVsFinDetails(periodName);
		System.out.println("completed dry run");
		
		
		//
	}

}
