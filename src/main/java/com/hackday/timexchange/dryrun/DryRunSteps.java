package com.hackday.timexchange.dryrun;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import jxl.Workbook;
import jxl.write.WritableWorkbook;

import com.hackday.timexchange.DBConnection;
import com.hackday.timexchange.util.ResultSetToExcel;

public class DryRunSteps {

	DBConnection dbAcess = 	DBConnection.getInstance();
	Connection con =  dbAcess.connect();
	Statement stmt;

	public void invokeSystemAdminBlackout() throws Exception {


		try {
			stmt = con.createStatement();
			int value = stmt.executeUpdate("exec security..ta_app_cntl_proc 'systemadmin', 'revoke'");

			if (value>0){
				Logger.getLogger("updated system to black out");
			}

			else {

				throw new Exception("Failed to put system in black out");
			}




		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
		}
	}

	public void fetchNonTMEstimatedAccruals() throws Exception{
		String aFileName = "c:\\output\\HD_NonTM_estimated_accruals.xls";
		WritableWorkbook workbook=Workbook.createWorkbook(new File(aFileName));
		try {
			stmt = con.createStatement();

			ResultSet rs  = stmt.executeQuery("user_db.dbo.HD_NonTM_estimated_accruals_proc");

			if(rs != null){
				// Call RS to Excel api
				new ResultSetToExcel().toXLS(rs, workbook.createSheet("Sheet 1",0));
				System.out.println("Created "+aFileName);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
			workbook.write();
			workbook.close();
		}
	}

	public void fetchPhyVsFinDetails(String peroid) throws Exception{
		String aFileName = "c:\\output\\HD_phy_Vs_fin.xls";
		WritableWorkbook workbook=Workbook.createWorkbook(new File(aFileName));
		try {
			stmt = con.createStatement();

			ResultSet rs  = stmt.executeQuery("user_db.dbo.HD_phy_fin_compare_proc '"+peroid+"'");
			if(rs != null){
				// Call RS to Excel api
				new ResultSetToExcel().toXLS(rs, workbook.createSheet("Sheet 1",0));
				System.out.println("Created "+aFileName);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
			workbook.write();
			workbook.close();
		}
	}

	public void runFinanacialProcess(String periodName) throws Exception {
		String interfacing_system_cd = "ISGL";
		int financial_group_key=1;

		stmt = con.createStatement();

		try {
			System.out.println("Running fin_accrual_ins_proc ...");
			stmt.execute("oms_lite.dbo.fin_accrual_ins_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_calc_accrual_Non_TM_proc ..."); 
			stmt.execute("oms_lite.dbo.fin_calc_accrual_Non_TM_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_cancel_delivery_ins_proc ...");
			stmt.execute("oms_lite.dbo.fin_cancel_delivery_ins_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_ins_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_ins_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_WAC_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_WAC_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_usg_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_usg_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_layer_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_layer_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fifo_cost_compl_mth_upd_proc ...");
			stmt.execute("oms_lite.dbo.fifo_cost_compl_mth_upd_proc '"+periodName+"'");
			System.out.println("Running fin_aggr_interface_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_interface_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_interface_usg_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_interface_usg_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_interface_upd_proc ...");
			stmt.execute("oms_lite.dbo.fin_interface_upd_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
			System.out.println("Running fin_aggr_bal_ins_proc ...");
			stmt.execute("oms_lite.dbo.fin_aggr_bal_ins_proc "+financial_group_key+",'"+interfacing_system_cd+"','"+periodName+"'" );
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			stmt.close();
		}

	}


}


