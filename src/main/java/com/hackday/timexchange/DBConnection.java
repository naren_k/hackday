package com.hackday.timexchange;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.sybase.jdbc3.jdbc.SybDriver;

public class DBConnection {

	public static DBConnection dbConnection = new DBConnection();
	String host = "nycpaperdevl.timeinc.com";
	String url = "jdbc:sybase:Tds:"+host+":5002";
	public static final  String username = "settyh";
	public static final  String password ="time_2015";
	SybDriver sybDriver = null;
	Connection conn = null;

	private DBConnection(){

	}

	public static DBConnection getInstance() {
		return dbConnection;
	}

	public Connection connect()
	{


		try 
		{
			sybDriver=(SybDriver)Class.forName("com.sybase.jdbc3.jdbc.SybDriver").newInstance();
			System.out.println("Driver Loaded");
			conn = DriverManager.getConnection(url,username,password);


		} 
		catch (InstantiationException ex) 
		{
			Logger.getLogger("");
		} 
		catch (IllegalAccessException ex) 
		{
			Logger.getLogger("");
		} 
		catch (ClassNotFoundException ex) 
		{
			Logger.getLogger("");
		} 
		catch (SQLException ex) 
		{
			Logger.getLogger("");
		}

		return conn;
	}

	public void closeConnection()  {
		try {
			conn.close();
		}
		catch (SQLException ex) 
		{
			Logger.getLogger("");
		}

	}



}

